import { Component, OnInit } from '@angular/core';
import { CONSTANTS } from 'src/app/shared/globals';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  apps = [
    { title: 'Movie App', link: CONSTANTS.MOVIE_APP_URL, desc: 'The top 10 movies from IDBD are listed here' },
    { title: 'Music App', link: CONSTANTS.MUSIC_APP_URL, desc: 'An application showing the list of random songs' }
  ];
  ngOnInit(): void {
  }

}
